import styled from 'styled-components'
import imgLogoHome from '../../images/logo-home.png'
import imgLogoNav from '../../images/logo-nav.png'
import noImage from '../../images/no-image.jpg'
import icoSearch from '../../images/ic-search.png'
const colorPrimary = "#383743"
const colorButtonEnter = "#57bbbc"

export const LogoHome = styled.div`
    height:72px;
    background: url(${imgLogoHome});
    background-position: center;
    background-size: contain;
    background-repeat:no-repeat; 
    margin: auto auto 10px auto;
`;
export const LoginContainer = styled.div`
    text-align:center;
    .row{height:100vh;}
    h1{
        font-weight:bold; 
    }
    form{ 
        input,.form-control:focus{
            background:transparent;
            color: ${colorPrimary};
            border-top:none;
            border-left:none;
            border-right:none;
            border-bottom: 1px solid ${colorPrimary};
            border-radius:0px;
            box-shadow: none;
            :focus{
                background:transparent; 
            }
        }
        button{
            background-color: ${colorButtonEnter};
            color:white;
            text-transform: uppercase;
            font-weight:bold; 
            width: 90%;
            :hover{
                color:white;
                opacity:0.6;
            }
        }
        .error_login{
            color:red;
        }
    }
`
export const HeaderStyle = styled.div`
        height:24vh;
        background-image: linear-gradient(173deg, #ed4b76, #bc3c67);
        justify-content: center;
        align-items: center;
        display: flex;
        .imgLogoNav{
            background-image: url(${imgLogoNav});
            background-position: center;
            background-size: contain;
            background-repeat:no-repeat;
            height:57px;
            width:50%;
        }
        .search_space {
            width:50px; 
            height: 50px;
        button{
                width: 100%;
                height: 100%;
                border:none;
                background:transparent;
                background-image: url(${icoSearch});
                background-position: center;
                background-size: contain;
                background-repeat:no-repeat;
                :focus{
                    outline: none;
                }
                :hover{
                    opacity:0.6;
                }
        }
    }
`
export const SearchButton = styled.div`
    input{
        border:none;
        width:100%;
        color:white;
        border-bottom: 1px solid white; 
        height: 60px;
        background:transparent;
        background-image: url(${icoSearch});
        background-position: left;
        background-size: contain;
        background-repeat:no-repeat;
        padding-left:60px
        :focus{
            outline: none;
        }
    }
`

export const HomeStyle = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height:76vh;
`
export const SearchStyle = styled.div`
    .infop{
        color:#8d8c8c;
        display:grid;
    }
    .enterprise:hover{
        opacity:0.7;
    }
    a{
        color: ${colorPrimary};
        :hover{
            color: initial;
            text-decoration: none;
        }
    }
` 
export const PhotoEnterprise = styled.div`
        background-image: ${props => `url(http://empresas.ioasys.com.br/${props.url})`};
        background-position: center;
        background-size: contain;
        background-repeat:no-repeat;
        width:100%;
        height:160px;
`
export const PhotoInfo = styled(PhotoEnterprise)` 
       background-image: ${props => `url(http://empresas.ioasys.com.br/${props.url}))`};
       max-height:160px;
       margin-bottom: 20px;
`
export const DetailsStyle = styled.SearchStyle