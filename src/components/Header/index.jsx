import React from 'react'
import { HeaderStyle, SearchButton } from '../StyledComponents'

const Header = ({ history, logo, inputSearch, onSearch }) => { 
    return (
        <HeaderStyle>
            {logo && <div className="imgLogoNav" />}
            {logo &&
                <div className="search_space">
                    <button onClick={() => history.push('/search')} />
                </div>}
            {inputSearch &&
                <SearchButton className="w-75">
                    <input type="text" name="search" placeholder="Pequisar" onChange={(e) => onSearch(e.target.value)} />
                </SearchButton>
            }
        </HeaderStyle>
    )
}
export default Header;