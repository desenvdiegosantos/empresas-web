import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { isAuthenticate } from '../services/auth';

// import { isAuthenticate } from '../pages/Login';

import Home from '../pages/Home'
import Login from '../pages/Login'
import Result from '../pages/Result'
import Details from '../pages/Details'

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        isAuthenticate() ? (
            <Component {...props} />
        ) : (
                <Redirect to={{ pathname: '/login', state: { from: props.localtion } }} />
            )

    )} />
)
const Routers = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/login" component={Login} /> 
                <PrivateRoute exact path="/" component={Home} />
                <PrivateRoute path="/search" component={Result} />
                <PrivateRoute path="/details/:id" component={Details} />
            </Switch>
        </BrowserRouter>
    )

}

export default Routers;
