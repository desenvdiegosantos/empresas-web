import React, { useState } from 'react'
import api from '../../services/api'
import { Row, Col, Form } from 'reactstrap';
import { LogoHome, LoginContainer } from '../../components/StyledComponents'

const Login = (props) => {
    const [email, setEmail] = useState("testeapple@ioasys.com.br")
    const [password, setPassword] = useState("12341234")
    const [error,setError] = useState()
    const { history } = props;

    const authHandler = async (e) => {
        e.preventDefault(); 
        await api.post('/users/auth/sign_in', {
            email: email,
            password: password
        }).then((response) => {
            const {headers} = response;
            localStorage.setItem("accessToken", headers['access-token']);
            localStorage.setItem("client", headers['client']);
            localStorage.setItem("uid", headers['uid']);    
            setError("");
            history.push("/"); 
        }).catch((err) => {
            setError("Email ou senha inválidos!");
            return null;
        });
    }
    return (
        <LoginContainer>
            <Row className="m-0 justify-content-center align-items-center" >
                <Col xs="6" sm="6" md="4" lg="3" xl="3">
                    <LogoHome />
                    <div className="col-12 mx-auto mt-5">
                        <h1 className="h4">BEM-VINDO AO EMPRESAS</h1>
                    </div> 
                    <Form>
                        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
                        <div className="form-group mt-3">
                            <input type="email" placeholder="E-mail*" required="true" className="form-control" name="username" onChange={e => setEmail(e.target.value)} value={email} />
                        </div>
                        <div className="form-group">
                            <input type="password" placeholder="Senha*" required="true" className="form-control" name="password" onChange={e => setPassword(e.target.value)} value={password} />
                        </div>
                        <span className="error_login">{error}</span>
                        <button type="submit" className="btn" onClick={(e) => authHandler(e)}>Entrar</button> 
                    </Form>
                </Col>
            </Row>
        </LoginContainer>
    )
}

export default Login