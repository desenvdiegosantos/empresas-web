import React from 'react'
import { HomeStyle } from '../../components/StyledComponents'
import Header from '../../components/Header'
const Home = ({ history }) => {
    return (
        <div>
            <Header logo={true} inputSearch={false} history={history}/>
            <HomeStyle>
                Clique na busca para iniciar.
        </HomeStyle>
        </div>
    )
}
export default Home;