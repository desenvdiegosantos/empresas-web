import React, { useState, useEffect, useMemo } from 'react'
import { Container, Row, Col, Card } from 'reactstrap';
import { SearchStyle,PhotoInfo } from '../../components/StyledComponents'
import Header from '../../components/Header'
import api from '../../services/api'
const Details = (props) => {
    const [company, setCompany] = useState([])
    const { id } = props.match.params
    useEffect(() => {
        async function fetchMyAPI() {
            const response = await api.get(`/enterprises/${id}`, {
                headers: {
                    'access-token': localStorage.getItem("accessToken"),
                    'client': localStorage.getItem("client"),
                    'uid': localStorage.getItem("uid")
                }
            })
            setCompany(response.data.enterprise)
        }
        fetchMyAPI();
    }, [id])
    return useMemo(() => {
        console.log(company['photo'])
        return (
            <SearchStyle>
                <Header logo={true} inputSearch={false} />
                <Container>
                    <Row>
                        <Col className="mt-4">
                            <Card className="p-4">
                                <Row>
                                    <Col className="photo_info">
                                        <PhotoInfo url={company['photo']}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="info_deta">
                                        <p className="infop">
                                            {company['description']}
                                        </p>

                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </SearchStyle>
        )
    }, [company])
}
export default Details;