import React, { useState, useEffect, useMemo } from 'react'
import { Container, Row, Col, Card } from 'reactstrap';
import { SearchStyle, PhotoEnterprise, HomeStyle } from '../../components/StyledComponents'
import Header from '../../components/Header'
import api from '../../services/api'
import { Link } from 'react-router-dom'
const Result = () => {
    const [searchValue, setSearchValue] = useState('');
    const [companies, setCompanies] = useState([])
    useEffect(() => {
        api.get(`/enterprises?name=${searchValue}`, {
            headers: {
                'access-token': localStorage.getItem("accessToken"),
                'client': localStorage.getItem("client"),
                'uid': localStorage.getItem("uid")
            }
        }).then((response) => {
            setCompanies(response.data.enterprises)
        })
            .catch(error => {
                console.log(error);
            })
    }, [searchValue])
    return useMemo(() => {
        return (
            <SearchStyle>
                <Header logo={false} inputSearch={true} onSearch={setSearchValue} />
                <Container>
                    <Row>
                        {companies.length > 0 ?
                            companies.map((company) => {
                                return (
                                    <Col key={company.id} xs="12" sm="12" md="12" lg="4" xl="4" className="mt-4 enterprise">
                                        <Link to={`details/${company.id}`}>
                                            <Card  >
                                                <Row className="p-4">
                                                    {company.photo &&
                                                        <Col>
                                                            <PhotoEnterprise url={company.photo} />
                                                        </Col>
                                                    }
                                                    <Col className="info_deta mx-0" >
                                                        <h3>{company.enterprise_name}</h3>
                                                        <p className="infop">{company.enterprise_type.enterprise_type_name}
                                                            <small>{company.country}</small>
                                                        </p>
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Link>
                                    </Col>
                                )
                            })
                            : <HomeStyle className="w-100">Nenhuma empresa encontrada!</HomeStyle>}
                    </Row>
                </Container>
            </SearchStyle>
        )
    }, [companies]);

}
export default Result